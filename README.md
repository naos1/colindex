[日本語](README_ja.md)

# colindex

 Python scripts for extracting, tracking, and plotting the extraction of low-pressure systems in the upper middle troposphere, called cutoff lows or cold (upper) vortices. The intensity, size, and center point of the "concavity" are measured geometrically from a 2-dimensional field of atmospheric data. Geopotential (height) can be used to extract cutoff lows and (blocking) highs, but other variables are also applicable. The scripts are corded using only the libraries initially provided with Anaconda, but an external library is used for plotting.

## Refference

- Kasuga, S., M. Honda, J. Ukita, S. Yaname, H. Kawase, A. Yamazaki, 2021: Seamless detection of cutoff lows and preexisting troughs. *Monthly Weather Review*, **149**, 3119–3134, [https://doi.org/10.1175/MWR-D-20-0255.1](https://doi.org/10.1175/MWR-D-20-0255.1)  

Please cite this paper when using this tool.


# Download and Installation

Go to any directory on the terminal and execute the following command to clone (download) this repository.

```
git clone https://gitlab.com/naos1/colindex.git
```

## Required Python version
Above 3.6 (but, No detailed verification has been done.)


## Requiered external packages

-  [Numpy](https://numpy.org/doc/stable/user/whatisnumpy.html)
-  [Pandas](https://pandas.pydata.org/docs/getting_started/overview.html)
-  [Matplotlib](https://matplotlib.org/stable/index.html)
-  [Cartopy](https://scitools.org.uk/cartopy/docs/latest/gallery/index.html)

# How to use

-  [Preprocess](md/preprocess.md)
-  [Making data_settings.py](md/data_settings.md)
-  [Run colindex.py](md/colindex.md)
-  [Plot detections](md/indexmap.md)  

## Quick reference for output csv
-  [Quick reference for output csv](md/variables.md)

## Quick reference of commands
```
./colindex.py -o dir_out
./colindex.py -i path/to/Z.bin -t yyyymmddhh -o dir_out
./indexmap.py -p dir_out -o dir_fig
./indexmap.py -i path/to/Z.bin -t yyyymmddhh -p dir_out -o dir_fig
```

dir_out: directory for outputs, dir_fig: directory for figures
Z.bin: input file, yyyymmddhh: year, month, day, and hour in 10 digits

## What's new
2023/02/08
- English version README has been created.

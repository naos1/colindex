# colindex

寒冷渦と呼ばれる対流圏中層上層の低気圧を抽出，追跡し，結果を描画するためのPythonスクリプト群です．格子状の大気データを入力しその2次元場より「凹み」の強度・サイズ・中心点を幾何学的に計測します．ジオポテンシャルやジオポテンシャル高度を入力すれば寒冷渦やブロッキング高気圧を抽出できますが，他の変数を入力することも可能です．抽出・追跡スクリプトはAnacondaに初期装備されているライブラリのみで構成されておりますが，入力データの前処理や結果の描画に一部の外部ライブラリを利用します．

## 参考文献

- Kasuga, S., M. Honda, J. Ukita, S. Yaname, H. Kawase, A. Yamazaki, 2021: Seamless detection of cutoff lows and preexisting troughs. *Monthly Weather Review*, **149**, 3119–3134, [https://doi.org/10.1175/MWR-D-20-0255.1](https://doi.org/10.1175/MWR-D-20-0255.1)  

本ツールのご利用におかれましては，上記のKasuga et al. (2021)の引用をお願いいたします．

# 導入
## ダウンロード

端末で任意のディレクトリへ移動し，以下のコマンドを実行しこちらのリポジトリをクローン（ダウンロード）します．

```
git clone https://gitlab.com/naos1/colindex.git
```

## Pythonのバージョン
Python3.6以上で動作するはずです．


## 必要な外部パッケージ

以下は利用するライブラリです．

-  [Numpy](https://numpy.org/doc/stable/user/whatisnumpy.html)
-  [Pandas](https://pandas.pydata.org/docs/getting_started/overview.html)
-  [Matplotlib](https://matplotlib.org/stable/index.html)
-  [Cartopy](https://scitools.org.uk/cartopy/docs/latest/gallery/index.html)


# 使い方

-  [データの前処理](md/preprocess_ja.md)
-  [設定ファイル(data_settings.py)の作成](md/data_settings_ja.md)
-  [渦の抽出(colindex.py)の実行](md/colindex_ja.md)
-  [抽出データの作図(indexmap.py)](md/indexmap_ja.md)

# 出力変数早見表
-  [出力変数早見表](md/variables.md)

# 実行コマンド早見表
```
./colindex.py -o dir_out
./colindex.py -i path/to/Z.bin -t yyyymmddhh -o dir_out
./indexmap.py -p dir_out -o dir_fig
./indexmap.py -i path/to/Z.bin -t yyyymmddhh -p dir_out -o dir_fig
```

dir_out: 抽出結果，dir_fig: 画像出力先，
Z.bin: 入力ファイル名，yyyymmddhh: 年月日時10ケタ，

## 更新情報
2023/02/23
- 英語版公開へ向けファイルを整理しました．

---
[新潟大サーバ](md/niigata.md)
#!/usr/bin/env python3
""" for colindex v1.2.7o """

import numpy as np

# input file (4-byte, big_endian)
infile = 'sample/Z2015041300-1406.bin'

# initial time-step YYYYMMDDHH
inittime = '2015041300'

# flags for detection
L_DETECT = True  # low detection
H_DETECT = True  # high detection
DISTINCT = True  # judge whether a extremum is in r_o*0.63
# test
PROCESS2 = False  # output minor intermediate products

CPU = 6  # number of multi-process (devide timesteps)

# radial serching variable [km]
rmin, rmax, rint = 100, 2600, 100

# grid configurations for the lat-lon coordinate
dx = 1.25  # longitudinal interval [deg]
dy = 1.25  # latitudinal interval [deg]
dt = 6  # timestep interval [hour]

# vertical layers
levs = np.array(  # [hPa]
    [1000, 975, 950, 925, 900, # [hPa]
     875, 850, 825, 800, 775,
     750, 700, 650, 600, 550,
     500, 450, 400, 350, 300,
     250, 225, 200, 175, 150,
     125, 100, 70, 50, 30, 20,
     10, 7, 5, 3, 2, 1])

selected_lev = [15, 19, 22]  # 500, 300, 200 hPa
#selected_lev = [x for x in range(len(levs))]  # all levels
#selected_lev = [x for x in range(14,28)]  # 550-70hPa

# factor for slope unit
# Geopotential height: [gpm]
# radial searching variable: [km]
# slope unit: [gpm / 100km] -> 1 / 100
factor = 1 / 100

# threshold for the slope ratio
SR_thres = 3.

# target area and levels
SP_S_lim, SP_N_lim = -75, -10
NP_S_lim, NP_N_lim = 10, 75

# constants
undef = 9.999e+20
rr = 6371.  # [km]
g = 9.8
omg = 7.292*1e-5

##########
# no need to edit below

r = np.arange(rmin, rmax+rint/2, rint)

lon = np.arange(0., 360, dx, dtype='f4')
lat = np.arange(-90, 90.01, dy, dtype='f4')

ix, iy = len(lon), len(lat) # grid nubmers

iz = len(levs) # level numbers

gss, gsn= int(*np.where(lat==SP_S_lim)), int(*np.where(lat==SP_N_lim))  # 12, 60
gns, gnn= int(*np.where(lat==NP_S_lim)), int(*np.where(lat==NP_N_lim))  # 84, 132
s_lat = range(gss-1, gsn+2)  # 11=-76.25S, 61=-14.75S
n_lat = range(gns-1, gnn+2)  # 83=13.75N, 133=76.25N
a_lat = np.append(s_lat, n_lat)
#a_lat = range(gss-1, gnn+2)  # allow at equator


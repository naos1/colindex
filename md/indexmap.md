# Plot extraction [indexmap.py](../indexmap.py)
It plots the extracted data and performs the same parallel processing as colindex.py. It also uses an external library called cartopy.

First, edit this file. Select the northern hemisphere (`np`), southern hemisphere (`sp`), or designated area (`area`) to `mproj` at the top of the file. if `area` is selected, edit the latitude and longitude variables just below `mproj`. Other settings are loaded from data_settings.py. Next, run it by the following commands.
```
./indexmap.py -p detect -o fig1
```
Use `-p` to select the directory where you have just saved the extracted data. Use `-o` to specify where to save the figures. The default for both is the current directory. As with colindex.py, -i and -t options are also available.


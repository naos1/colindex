# Preprocess
The current version uses a flat binary format (4 bytes, big_endian) for input and output. The data is arranged in a Python-like array, arranging from the "inside",

- eastward axis (from west to east)
- northward axis (from south to north)
- vertical axis (either way)
- time axis (either way)

This structure can be said as a GrADS-friendly format.

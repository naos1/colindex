# Quick look for output csv

| index    | field name   | description                                                                                                                   |
| -------- | ------------ | ----------------------------------------------------------------------------------------------------------------------------- |
| 0,1,2,3  | Y,M,D,H      | year, month, day, hour                                                                                                        |
| 4,5,6,7  | lat,lon,y,x  | latitude, longitude, index for lat, lon                                                                                       |
| 8        | val          | extracted point value of the input field                                                                                      |
| 9,10,11  | So, ro, Do   | optimal slope [gpm / 100 km], optimal radius[km], optimal depth[gpm]                                                          |
| 12,13    | Sb, ang      | background (BG) slope [gpm / 100km], direction of BG gradient [rad] (0 toward east)                                           |
| 14,15    | m, n         | eastward and northward component of BG slope [gpm / 100km]                                                                    |
| 16       | SR           | slope ratio（S_BG/S_o）                                                                                                       |
| 17       | ex           | judge ment of extremum<br>1：there is/are more than one local extremum within the circle of r_o\*0.63<br>0：otherwise         |

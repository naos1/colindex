# How to edit data_settings.py

First, copy data_settings.py from data_settings.org.py.

```
$ cp data_settings.org.py data_settings.py
```

Any further modifications must be made to data_settings.py

- `infile`  
Specifies the file path of the input data. The data format is 4-byte, big endian, flat binary on a latitude-longitude grid. The data must be structured in axis order, eastward -> northward -> vertical -> time. The vertical layer and time step can be one.

- `inittime`  
Specify the initial time of the input data in the format YYYYYMMDDHH. (4-digit year, 2-digit month, 2-digit day, 2-digit hour)

- `L_DETECT`, `H_DETECT`  
Flag for extracting lows and/or highs, set True or False. The file name of the extracted data will start with "V-L-" for lows and "V-H-" for highs.

- `X_DETECT`  
Extraction flags for the minima and maxima, which are linked to the `L_DETECT` and `H_DETECT` flags, respectively. The minimum and maximum points are output with file names starting with "X-L-" and "X-H-", respectively.

- `DISTINCT`  
Determines if there is a extremum point inside each vortex.

- `PROCESS2` (test)  
Outputs the intermediate generated data. False is recommended since they will be large data. Outputs $`r`$ and the corresponding $`m,n`$ 2D distribution satisfying $`AS^+(x,y) = \underset{r}{max} AS(r;x,y)`$ at each grid point.

- `CPU`  
Specifies the number of parallel computations. It is recommended that the number is less than or equal to the machine's maximum number of available cores and a common divisor of the time step.

- `rmin`, `rmax`, `rint`  
Specifies the search range in km for estimating the phenomenon size. `rmin` is the minimum, `rmac` is the maximum, and `rint` is the step interval. In Kasuga et al. (2021), these are set as
```
rmin, rmac, rint = 200, 2100, 100
```
Adjust accordingly when adapting to blocking highs or other phenomena (typhoons, meso-lows, etc.).

- `dx`, `dy`, `dt`    
Specify the longitude (degree), latitude (degree), and time step (hour) intervals for the force data, respectively.

- `levs`  
Specify the vertical layer array (Ndarray) corresponding to the vertical layer of the input data. This array is not used in the calculation, but is used for file names.

- `selected_lev`  
Select the vertical layer from which to perform the extraction, specifying the index of the levs. Note that it starts from zero.

- `factor`  
A coefficient to adjust the units of the output value.

- `SR_thres`  
The threshold for denoising by slope ratio ($`SR = S_o / S_{BG}`$). 3 is recommended.

- `SP_S_lim`, `SP_N_lim`  
Specifies the southern latitude zone for the extraction. The current version does not allow extraction near the poles, so the recommended value for the southern limit (SP_S_lim) is -75 (degrees). Note that geostrophic winds cannot be defined, although they can be calculated at the equator.

- `NP_S_lim`, `NP_N_lim`  
Same as above. But for the Northern Hemisphere.

- `undef`, `rr`    
undefined value and earth radius km.

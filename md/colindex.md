# Tutorial to use [colindex.py](../colindex.py)

Execute the following command in the top-level directory. In this case, `infile` (input file path) and `inittime` (data start time) in data_settings.py are referenced.
```
./colindex.py -o detect
```
The output directory can be specified with `-o` (the detect directory will be generated). If not specified, output is made to the current directory. At the same time, a copy of data_settings.py is saved so that the extraction settings can be checked later. The input file path can be specified with option `-i` and also be specified in 10-digit with option `-t`.

```
./colindex.py -i sample/Z2015041300-1406.bin -t 2015041312 -o detect
```
In this case, `infile` and `inittime` written in data_settings.py will be ignored.

# Output files
There are three basic files, starting with `V-`, `X-`, and `AS-`. `-L-` is for low and `-H-` is for high. `yyyymmddhh` is year 4 digits, month 2 digits, day 2 digits, and hour 2 digits. `lllll` is for vertical (i.e., pressure) level in 4 digits.

## V-L-yyyymmddhh-llll.csv, V-H-yyyymmddhh-llll.csv  
Data for the extraction point. A brief description of each field is summarized in the table below.

|Y  |M  |D  |H  |lat|lon|y  |x  |val|So |ro |Do |Sb |ang|m  |n  |SR |ex|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|year |month |day |hour|latitude|longitude|index of lat|index of lon|point value of input|optimal slope|optimal radius|optimal depth|BG slope|its direction|eastward BG slope|northward BG slope|slope ratio|extremum judge|

The extremum points are searched for within a circle of ro*0.63 radius at the low or high. If there are more than one extremum, get 1, otherwise get 0. (In the case of cutoff low extraction, ex=0 is a trough and ex=1 is a cutoff low). See the references for details.


## X-L-yyyymmddhh-llll.csv, X-H-yyyymmddhh-llll.csv
Data of minima. Only the points inside the circle of ro*0.63 radius are extracted. The description of each field is summarized in the table below.

|Y  |M  |D  |H  |lat|lon|y  |x  |val|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|year |month |day |hour|latitude|longitude|index of lat|index of lon|point value of input|

## AS-L-yyyymmddhh-llll.csv, AS-H-yyyymmddhh-llll.csv
$`AS^+`$ for low detection or $`AS^-`$ for high detection are stored in 4-byte big-endian flat binary format. When using GrADS, please use the templated ctl file ([AS-L-0200.ctl](AS-L-0200.ctl)).


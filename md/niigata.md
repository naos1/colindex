# アーカイブデータの保存場所（新潟大サーバ，サーバ名:tornado1）
-  /mnt/data4/JRA-55-6hourly-month/HGTyyyymm.bin
1958–2020のJRA55ジオポテンシャル高度をバイナリ（4byte, big endian）に変換し月毎にまとめたもの(yyyy:年,mm:月)  
ダウンロード例
```
scp xxx.xx.xx.xxx:/mnt/data4/JRA-55-6hourly-month/HGT201504.bin .
```

-  /mnt/data1/colindex_archive/clim-v1.2.6o-DR0.63/yyyymm
上記データ全期間の550–150 hPaを用いてcolindex.pyを実行した抽出結果（高低気圧，100<=r<=2600 km，DR=0.63）  
ダウンロード例
```
scp -r xxx.xx.xx.xxx:/mnt/data1/colindex_archive/clim-v1.2.6o-DR0.63/201001/V-L-\*0200.csv .
```

-  /mnt/data1/colindex_archive/clim-v1.2.6o-DR0.63_t1.9.3-So5_cd1.0-70/yyyymm
上記データより追跡と寿命数え上げを行ったもの．  
ダウンロード例
```
scp -r xxx.xx.xx.xxx:/mnt/data1/colindex_archive/clim-v1.2.6o-DR0.63_t1.9.3-So5_cd1.0-70/201504/V-L-\*0200.csv .
```

-  /mnt/data1/colindex_archive/clim-v1.2.6o-DR0.63_t1.9.3-So5_cd1.0-70_ID0.4-JPN/'ty'-'lev'
上記データより，日本周辺（135ºE, 35ºNを中心とした1000km円内）を通過した寒冷渦のIDfile．  
ダウンロード例
```
scp -r xxx.xx.xx.xxx:/mnt/data1/colindex_archive/clim-v1.2.6o-DR0.63_t1.9.3-So5_cd1.0-70_ID0.4-JPN/L-200 .
```

#!/usr/bin/env python3
""" Cutoff low and Blocking high detection scheme v1.2.7

Publication: Kasuga et al. 2021, Monthly Weather Review.

Copyright (C) 2018-2021 Satoru Kasuga or NAOS, Niigata univ. All rights reserved.
"""

# import variables in data_settings.py
from data_settings import *

# get argument with argparse
from argparse import ArgumentParser
epi0 = 'Please set appropriate parameters in data_settings.py before run this script.'
epi1 = 'When you run, note below:'
epi2 = 'If you skip argument -i, "infile" in data_settings.py will be used.'
epi3 = 'If you skip argument -t, "inittime" in data_settings.py will be used.'
ap = ArgumentParser(usage='{} [-i <path>] [-t <yyyymmddhh>] [-o <path>] [-h]'.format(__file__),
                    epilog=f'{epi0} {epi1} {epi2} {epi3}')

ap.add_argument('-i', type=str, metavar='INPUT_PATH',
                help='input data path')
ap.add_argument('-t', type=str, metavar='INIT_TIME',
                help='initial time formatted in yyyymmddhh')
ap.add_argument('-o', type=str, metavar='OUTPUT_PATH', default='.',
                help='specify output directory (default: current dir.)')
args = ap.parse_args()

if args.i != None:
    infile = args.i
elif infile == '':
    print('Specify input data in data_settings.py or with argument -i <path>')
    quit()

if args.t != None:
    _t = args.t
elif inittime == '':
    print('Specify tinitial time in data_settings.py or with argument -t <YYYYMMDDHH>')
    quit()
else:
    _t = inittime

if args.o == None:
    odir = '.'
else:
    odir = args.o

# set initial time of data
import datetime
init_time = datetime.datetime(int(_t[:4]), #year
                              int(_t[4:6]), #month
                              int(_t[6:8]), #day
                              int(_t[8:10])) #hour

import warnings
warnings.simplefilter('ignore')

# import standard libraries
import sys
import os
import itertools
import shutil
import multiprocessing as mp
from collections import OrderedDict

# import external libraries
import numpy as np
import pandas as pd


# make output directory
os.makedirs(odir, exist_ok=True)
# copy data_settings.py in output directory
if args.i == None:
    now = datetime.datetime.now()
    _y, _m, _d, _h, _mi = now.year, now.month, now.day, now.hour, now.minute
    shutil.copy('data_settings.py', f'{odir}/data_settings_{_y}{_m:02}{_d:02}{_h:02}{_mi:02}.py')


def open_getit(infile):
       
    data_size = os.path.getsize(infile)

    if data_size % (ix * iy * iz * 4) != 0:
        print('your file size is not consistent with data_settings.py!')
        quit()

    else:
        a = open(infile, 'rb')
        it = data_size / (ix * iy * iz * 4)

        data = np.fromfile(a, dtype='>f4').reshape((int(it), iz, iy, ix))
        data = np.where(data==undef, np.nan, data)
        a.close
    
    return data, int(it)


def check_zonal_loop():

    # starting around zero longitude
    if lon[0] - dx <= 0. or lon[-1] + dx >= 360.:
        return True

    # not around zero longitude
    elif lon[0] > lon[-1] and lon[0] - dx <= lon[-1]:
        return True
    
    # regional data
    else:
        return False


def check_meridional_loop():

    # need also zonal loop
    if not check_zonal_loop():
        return False

    if lat[0] - dy <= -90. and lat[-1] + dy >= 90.:
        return True

    else:
        return False


def get_interp_zonal(zz, r):

    east = np.full((iy, ix), np.nan, dtype='f4') # east
    west = np.full((iy, ix), np.nan, dtype='f4') # west
    
    for y in a_lat :
        
        # dist of adjacent zonal grids
        dist = 2 * np.pi * rr * np.cos(np.pi*lat[y]/180) * dx / 360
        
        # interpolate points for each lat
        last_dist = 0.
        for g in range(10000) :                
            _dist = dist * g

            if _dist > int(r) :
                dg = g
                break

            last_dist = _dist
        
        # make widen field (zw)
        zw = np.empty((ix+dg*2), dtype='f4')
        z = zz[y, :]
        
        if check_zonal_loop():   # zonal loop
            zw[:dg] = z[-dg:]    #  *E*,--------,---
            zw[dg:ix+dg] = z[:]  #  ---,*W****E*,---
            zw[ix+dg:] = z[:dg]  #  ---,--------,*W*

        else:                    # not zonal loop
            zw[:dg] = np.nan     #  uuu,--------,--- u: undef
            zw[dg:ix+dg] = z[:]  #  ---,********,---
            zw[ix+dg:] = np.nan  #  ---,--------,uuu

        del z
        
        # west2 west1 .... center .... east1 east2
        west2, west1 = zw[:-dg*2],    zw[1:-dg*2+1]
        east1, east2 = zw[dg*2-1:-1], zw[dg*2:]
        del zw
        
        # zonal liner interpolation
        A = _dist - int(r)
        B = int(r) - last_dist
        _west = (A*west1 + B*west2) / (A+B) 
        _east = (A*east1 + B*east2) / (A+B) 
        
        # remove undef 
        _west = np.where((np.isnan(west1))|(np.isnan(west2)), np.nan, _west)
        _east = np.where((np.isnan(east1))|(np.isnan(east2)), np.nan, _east)
        del west1, west2, east1, east2
        
        east[y,:] = _east
        west[y,:] = _west
        del _east, _west

    return east, west


def get_interp_meridional(z, r):

    dist_lat = 2 * np.pi * rr *dy / 360 # constant
    rp = iy//3 # wrapping grid number

    # search interpolate points
    last_dist = 0.
    for g in range(1, 100) :
        _dist = dist_lat * g

        if _dist > int(r) :
            dg = g
            break

        last_dist = _dist

    # make widen field (zw)
    zw = np.empty((iy+rp*2, ix), dtype='f4')

    if check_meridional_loop():

        # make polar loop
        _zrev = np.empty((iy, ix), dtype='f4')
        zrev = z[::-1, :]    # reverse S->N into N->S

        # slide 180 deg
        if ix % 2 == 0:
            # 1-144, 145-288 ==> :144, 144:
            _zrev[:, :ix//2] = zrev[:, ix//2:]
            _zrev[:, ix//2:] = zrev[:, :ix//2]
        else :
            _zrev[: ,:ix//2] = zrev[:, ix//2+1:]
            _zrev[: ,ix//2+1:] = zrev[:, :ix//2]

        zw[-rp:, :] = _zrev[1:rp+1, :]     #  *N*,---------,---
        zw[rp:-rp, :] = z[:, :]            #  ---,*N*****S*,--- 
        zw[:rp, :] = _zrev[-(rp+1):-1, :]  #  ---,---------,*S*

        del _zrev, zrev

    else: # not meridional loop
        zw[-rp:, :] = np.nan               #  uuu,---------,--- u: undef
        zw[rp:-rp, :] = z[:, :]            #  ---,*N*****S*,--- 
        zw[:rp, :] = np.nan                #  ---,---------,uuu

        del z,

    # north2 north1 .... center .... south1 south2
    north2, north1 = zw[rp+dg:rp+dg+iy, :], zw[rp+dg-1:rp+dg+iy-1 ,:]
    south1, south2 = zw[rp-dg+1:rp-dg+iy+1, :], zw[rp-dg:rp-dg+iy, :]

    # meridional liner interpolation
    A = _dist - int(r)
    B = int(r) - last_dist
    _north = (A*north1 + B*north2) / (A + B)
    _south = (A*south1 + B*south2) / (A + B)

    # remove undef
    _north = np.where((np.isnan(north1))|(np.isnan(north2)), np.nan, _north)
    _south = np.where((np.isnan(south1))|(np.isnan(south2)), np.nan, _south)
    del north1, north2, south1, south2

    return _north, _south


def tripod_approximation(n, s, e, w):
    """
    Approximate a missing point value
    with other valid three point values.

    Note
    ----
    You can chose whether to use this scheme
    by the "TRIPODAP" flag in data_settings.py.

    """
    n_not = np.logical_not(np.isnan(n))
    s_not = np.logical_not(np.isnan(s))
    e_not = np.logical_not(np.isnan(e))
    w_not = np.logical_not(np.isnan(w))
    
    n = np.where((np.isnan(n))|(s_not)|(e_not)|(w_not), e+w-s, n)
    s = np.where((np.isnan(s))|(n_not)|(e_not)|(w_not), e+w-n, s)
    e = np.where((np.isnan(e))|(w_not)|(n_not)|(s_not), n+s-w, e)
    w = np.where((np.isnan(w))|(e_not)|(n_not)|(s_not), n+s-e, w)

    return n, s, e, w


def envelope_averaged_slope_func(f, ty):
    """
    Calculate envelope of AS.

    Note
    ----
    When you use high-detection mode,
    the lower envelope fields has inverted signs,
    for a simple code to search local maxima.    
    """

    buf = -9.999e+20

    AS_en = np.full((iy, ix), buf, dtype='f4')
    r_en = np.empty((iy, ix), dtype='f4')
    m_en = np.empty((iy, ix), dtype='f4')
    n_en = np.empty((iy, ix), dtype='f4')
    
    for _r in r:

        e, w = get_interp_zonal(f, _r)
        n, s = get_interp_meridional(f, _r)

        if TRIPODAP:
            n, s, e, w = tripod_approximation(n ,s, e, w)

        AS_r = (n+s+e+w-4*f) / factor / 4 / _r
        m_r = (e-w) / factor / 2 / _r
        n_r = (n-s) / factor / 2 / _r

        # sign inversion for lower envelope
        if ty == 'H':
            #AS_r = np.where(AS_r != undef, AS_r*-1, undef)
            AS_r = AS_r * -1

        # check endian
        if np.any(AS_r > 100000000.) or np.any(AS_r < -100000000.):
            print('error: please check endian of input data')
            quit()
            
        # search envelope
        r_en = np.where(AS_r >= AS_en, int(_r), r_en)
        m_en = np.where(AS_r >= AS_en, m_r, m_en)
        n_en = np.where(AS_r >= AS_en, n_r, n_en)
        AS_en = np.where(AS_r >= AS_en, AS_r, AS_en)        

    AS_en = np.where(AS_en == buf, np.nan, AS_en)
    
    return AS_en, r_en, m_en, n_en


def output_bin(obin, oname, tt, l, ty):

    obin = np.where(np.isnan(obin), undef, obin)

    if oname == "AS":
        obin = np.where(obin<0., 0., obin)

    td = f'{tt.year}{tt.month:02}'
    ts = f'{td}{tt.day:02}{tt.hour:02}'

    if not os.path.isdir(f'{odir}/{td}'):
        os.makedirs(f'{odir}/{td}', exist_ok=True)
    
    #out = open(f'{odir}/{oname}-{ty}-{ts}-{levs[l]:04}.grd', 'wb')
    out = open(f'{odir}/{td}/{oname}-{ty}-{ts}-{levs[l]:04}.grd', 'wb')
    obin = np.array(obin, dtype='>f4')
    out.write(obin)
    out.close
    del obin


def search_localextremums(f, maxmin):

    # Bool for local maximum points
    lmp = np.full((iy, ix), False)
    wide = np.empty((iy, ix+2), dtype='f4')
    
    # zonal loop
    wide[:, :1] = f[:, -1:]
    wide[:, 1:-1] = f[:, :]
    wide[:, -1:] = f[:, :1]
    
    # center 
    o = f[1:-1, :]
    # around 8 grids
    n = wide[ 2:  , 1:-1] - o
    s = wide[  :-2, 1:-1] - o
    w = wide[ 1:-1,  :-2] - o
    e = wide[ 1:-1, 2:  ] - o
    ne = wide[ 2:  , 2:  ] - o
    se = wide[  :-2, 2:  ] - o
    nw = wide[ 2:  ,  :-2] - o
    sw = wide[  :-2,  :-2] - o
    
    if maxmin == "maximum":
        # search local maximum points  ## note undef=9.999e+20
        lmp[1:-1, :] = (n<=0)&(s<=0)&(w<=0)&(e<=0)&(ne<=0)&(se<=0)&(nw<=0)&(sw<=0)

    elif maxmin == "minimum":
        #lmp = np.where(lmp==undef, -undef, lmp)
        lmp[1:-1, :] = (n>=0)&(s>=0)&(w>=0)&(e>=0)&(ne>=0)&(se>=0)&(nw>=0)&(sw>=0)

    return lmp


def func_gcd(_lon1,_lat1,_lon2,_lat2) :
    """calculate great-circle distance [km]"""
    if _lon1 == _lon2 and _lat1 == _lat2 :
        return 0.
    else :
        lon1=_lon1*np.pi/180 ; lon2=_lon2*np.pi/180
        lat1=_lat1*np.pi/180 ; lat2=_lat2*np.pi/180                
        ds=np.arccos(np.sin(lat1)*np.sin(lat2)
                     +np.cos(lat1)*np.cos(lat2)*np.cos(abs(lon1-lon2)))
        return rr*ds 


def extract_optimal_parameters(p, f, dfx, tt, l, ty):

    # unpack envelope products
    AS_en, r_en, m_en, n_en = p

    # search_localextremums returns Bool array
    lmp = search_localextremums(AS_en, 'maximum')

    # noise reduction by the three constraints
    #############
    #lmp = np.where((r_en==rmin), False, lmp)  # use for tracking
    lmp = np.where(AS_en<=0., False, lmp)
    _AS_en = np.where(AS_en <= 0., np.nan, AS_en)  # prevent overflow
    #_m_en = np.where(m_en >= 100., 100., m_en)  # prevent overflow
    #_n_en = np.where(n_en >= 100., 100., n_en)  # prevent overflow
    #lmp = np.where(np.sqrt(_m_en**2+_n_en**2)/_AS_en>SR_thres, False, lmp)
    lmp = np.where(np.sqrt(m_en**2+n_en**2)/_AS_en>SR_thres, False, lmp)
    del _AS_en#, _m_en, _n_en
    #############

    # make point data
    ls_y, ls_x = np.where(lmp)
    ls_lat, ls_lon = lat[ls_y], lon[ls_x]

    # extract field values at points
    ls_val = f[(np.where(lmp))]
    
    # optimal slope #[gpm/100km]
    ls_So = AS_en[(np.where(lmp))] 
    # optimal radius #[km]
    ls_ro = r_en[(np.where(lmp))] 
    # optimal depth #[gpm]
    ls_Do = ls_So * ls_ro * factor
    
    ls_m = m_en[(np.where(lmp))] #[gpm/100km]
    ls_n = n_en[(np.where(lmp))] #[gpm/100km]
    # local backgroud slope [gpm/100km]
    ls_Sb = np.sqrt(ls_m**2+ls_n**2) 
    # local background grad. angle [rad]
    ls_ang = np.arctan2(ls_n, ls_m) 
    
    # slopeo ratio
    ls_SR = ls_Sb / ls_So

    # datetime
    ls_yy = np.full(ls_y.shape, f'{tt.year:04}')
    ls_mm = np.full(ls_y.shape, f'{tt.month:02}')
    ls_dd = np.full(ls_y.shape, f'{tt.day:02}')
    ls_hh = np.full(ls_y.shape, f'{tt.hour:02}')
    td = f'{tt.year}{tt.month:02}'
    ts = f'{td}{tt.day:02}{tt.hour:02}'

    # ro-area local AS maximum flag
    ls_d = np.zeros((len(ls_x)))
    # ro-area input field extremum flag
    ls_ex = np.zeros((len(ls_x)))
    
    # all in one dictionary for DataFrame of point data
    dic = OrderedDict([('Y',ls_yy), ('M',ls_mm), ('D',ls_dd), ('H',ls_hh),
                       ('lat',ls_lat), ('lon',ls_lon),
                       ('y',ls_y), ('x',ls_x), ('val',ls_val),
                       ('So',ls_So), ('ro',ls_ro), ('Do',ls_Do),
                       ('Sb',ls_Sb), ('ang',ls_ang),
                       ('m',ls_m), ('n',ls_n), ('SR',ls_SR),
                       ('ex',ls_ex), ('d',ls_d)])
    df=pd.DataFrame.from_dict(dic)
    df=df.astype({'y':int, 'x':int, 'ro':int, 'ex':int, 'd':int})

    ###############
    # ro-area local maximum

    df = df.sort_values('So', ascending=False).reset_index(drop=True)

    for i in range(len(df)):

        if df.loc[i, ('d')] == 1:
            continue

        for j in range(i+1, len(df)):

            gcd = func_gcd(df.loc[i, ('lon')], df.loc[i, ('lat')],
                           df.loc[j, ('lon')], df.loc[j, ('lat')],)

            if gcd < df.loc[i, ('ro')] and gcd < df.loc[j, ('ro')]:

                if df.loc[j, ('So')] <= df.loc[i, ('So')]:
                    # not area local maximum
                    df.loc[j, ('d')] = 1

    df = df[df['d'] == 0].reset_index(drop=True)

    # check and delete omega-shaped-height detections
    # under test
    if OMEGARMV:
        for i in range(len(df)):

            for j in range(len(df)):

                if i == j: continue

                gcd = func_gcd(df.loc[i, ('lon')], df.loc[i, ('lat')],
                               df.loc[j, ('lon')], df.loc[j, ('lat')],)

                if gcd < df.loc[i, ('ro')] and\
                   df.loc[i, ('So')] < df.loc[j, ('So')] and\
                   df.loc[i, ('ro')] > df.loc[j, ('ro')]:
                    if ty == 'L' and\
                       df.loc[i, ('val')] > df.loc[j, ('val')]:
                        # might be omega-shaped height
                        df.loc[i, ('d')] = 1
                    if ty == 'H' and\
                       df.loc[i, ('val')] < df.loc[j, ('val')]:
                        # might be omega-shaped height
                        df.loc[i, ('d')] = 1

    df = df[df['d'] == 0].reset_index(drop=True)

    # check ro-area extremum of input
    if DISTINCT:
        x_list = []
        for i in range(len(df)):
            dist_min = 100000.
            min_j = -1
            for j in range(len(dfx)):

                gcd = func_gcd(df.loc[i, ('lon')], df.loc[i, ('lat')],
                               dfx.loc[j, ('lon')], dfx.loc[j, ('lat')],)

                if gcd < df.loc[i, ('ro')]*0.63:  # =DR_c, critical value of gcd/ro
                    df.loc[i, ('ex')] = 1
                    if gcd < dist_min:
                        min_j = j
                        dist_min = gcd

            if not min_j == -1:
                x_list.append(min_j)

        dfx = dfx.iloc[x_list].reset_index(drop=True)  # reject irrelevant local mins

    if not os.path.isdir(f'{odir}/{td}'):
        os.makedirs(f'{odir}/{td}', exist_ok=True)

    #dfx.to_csv(f'{odir}/X-{ty}-{ts}-{levs[l]:04}.csv',
    dfx.to_csv(f'{odir}/{td}/X-{ty}-{ts}-{levs[l]:04}.csv',
               header=True, index=False, float_format='%.4f')

    # output point data
    df = df.drop(columns=['d'])
    #df.to_csv(f'{odir}/V-{ty}-{ts}-{levs[l]:04}.csv',
    df.to_csv(f'{odir}/{td}/V-{ty}-{ts}-{levs[l]:04}.csv',
              header=True, index=False, float_format='%.4f')
    

def extract_input_extremum(f, tt, l, ty):

    if ty == 'L':
        maxmin = 'minimum'

    if ty == 'H':
        maxmin = 'maximum'

    lmp = search_localextremums(f, maxmin)

    # make point data
    ls_y, ls_x = np.where(lmp)
    ls_lat, ls_lon = lat[ls_y], lon[ls_x]

    # extract field values at points
    ls_val = f[(np.where(lmp))]

    # datetime
    ls_yy = np.full(ls_y.shape, f'{tt.year:04}')
    ls_mm = np.full(ls_y.shape, f'{tt.month:02}')
    ls_dd = np.full(ls_y.shape, f'{tt.day:02}')
    ls_hh = np.full(ls_y.shape, f'{tt.hour:02}')
    ts = f'{tt.year:04}{tt.month:02}{tt.day:02}{tt.hour:02}'

    dic = OrderedDict([('Y',ls_yy), ('M',ls_mm), ('D',ls_dd), ('H',ls_hh),
                       ('lat',ls_lat), ('lon',ls_lon),
                       ('y',ls_y), ('x',ls_x), ('val',ls_val)])
    df = pd.DataFrame.from_dict(dic)
    df = df.astype({'y':int, 'x':int})

    return df


def detect_out(d2, ite, c, l, ty):

    _t, _, _ = d2.shape

    '''
    if c != -1:
        print(levs[l], f'{c}/{CPU}', ty)
    else:
        print(levs[l], f'mod/{CPU}', ty)
    '''

    for t in range(_t):

        f = d2[t]

        if c != -1:
            tt = init_time + datetime.timedelta(hours=(ite*c+t)*dt)
        else:
            tt = init_time + datetime.timedelta(hours=(ite+t)*dt)
        
        processed_2d = envelope_averaged_slope_func(f, ty)
        processed_2d_names = ['AS', 'r', 'm', 'n']
        
        dfx = extract_input_extremum(f, tt, l, ty)
            
        for out_bin, out_name in zip(processed_2d, processed_2d_names):
            output_bin(out_bin, out_name, tt, l, ty)
            
            if not PROCESS2:
                break

        extract_optimal_parameters(processed_2d, f, dfx, tt, l, ty)


if __name__ == '__main__' :

    data, it = open_getit(infile)

    if CPU == None:
        CPU = 1

    rCPU = range(CPU)

    ite, mod = divmod(it, CPU)
    
    # main loop
    for i, l in enumerate(selected_lev):

        print(f'calculating @ {levs[l]} hPa ({i+1}/{len(selected_lev)})')

        # select a 2D field
        d2 = data[:, l, :, :].reshape((it, iy, ix))

        d2l = np.empty((CPU, ite, iy, ix))

        for c in rCPU:
            d2l[c,:,:,:] = d2[ite*c:ite*(c+1),:,:]
        d2l_mod = d2[ite*CPU:,:,:]
        
        ### core scheme
        if L_DETECT is True:

            with mp.Pool(CPU) as pool:
                exe = [pool.apply_async(detect_out, (d2l[c], ite, c, l, 'L')) for c in rCPU]
                [ff.get() for ff in exe]

            detect_out(d2l_mod, ite*CPU, -1, l, 'L')

                
        if H_DETECT is True:

            with mp.Pool(CPU) as pool:
                exe = [pool.apply_async(detect_out, (d2l[c], ite, c, l, 'H')) for c in rCPU]
                [ff.get() for ff in exe]                

            detect_out(d2l_mod, ite*CPU, -1, l, 'H')

    print('FINISH!')

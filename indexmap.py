#!/usr/bin/env python3
"""
draw index map with Cartopy
version 1.3

# 2019.02.17 ks
# 2019.04.19 ks
# 2019.08.05 ks
# 2019.09.19 ks
# 2020.08.23 ks
# 2021.02.13 ks
# 2021.04.20 ks
"""

# set map projection and area
# map projection
# "area": areal map (Stereographic)
# "np": north polar map (AzimuthalEquidistant)
# "sp": south polar map (AzimuthalEquidistant)
mproj = "np"

# map extention valid only when mproj = "area"
wlon, elon = 50, 215
slat, nlat = 15, 65

# center of projection
clon=(elon+wlon)/2
clat=(nlat+slat)/2

from data_settings import *

from argparse import ArgumentParser
ap = ArgumentParser(usage='{} [-i <path>] [-p <path>] [-t <yyyymmddhh>] [-o <path>] [-h]'.format(__file__))
ap.add_argument('-i', type=str, metavar='INdata',
                help='source input field data path')
ap.add_argument('-p', type=str, metavar='Pdir',
                help='product data path')
ap.add_argument('-t', type=str, metavar='INIT_TIME',
                help='initial time formatted in yyyymmddhh')
ap.add_argument('-o', type=str, metavar='PATH', default='.',
                help='specify output figure directory (default: current dir.)')
args = ap.parse_args()

if args.i != None:
    infile = args.i
elif infile == '':
    quit()

if args.t != None:
    _t = args.t
elif inittime == '':
    quit()
else:
    _t = inittime

if args.p == None:
    idir = '.'
else:
    idir = args.p

if args.o == None:
    odir = '.'
else:
    odir = args.o

import warnings
warnings.simplefilter('ignore')

import datetime
init_time = datetime.datetime(int(_t[:4]), #year
                              int(_t[4:6]), #month
                              int(_t[6:8]), #day
                              int(_t[8:10])) #hour

import sys
import os
os.makedirs(odir, exist_ok=True)

import multiprocessing as mp

import numpy as np
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as pat
import matplotlib.cm as cm
import matplotlib.path as mpath
import matplotlib.gridspec as gridspec

import mpl_toolkits.axes_grid1

import cartopy.crs as ccrs
import cartopy.feature as cftr
from cartopy.util import add_cyclic_point


def open_getit(infile):
       
    data_size = os.path.getsize(infile)

    if data_size % (ix * iy * iz * 4) != 0:
        print('your file size is not consistent with data_settings.py!')
        quit()

    else:
        a = open(infile,'rb')
        it = data_size / (ix * iy * iz * 4)

        data = np.fromfile(a, dtype='>f4').reshape((int(it), iz, iy, ix))
        a.close
    
    return data, int(it)


def make_blank():

    fig = plt.figure(figsize=(8, 8))

    # gridspec (coltrol axes)
    #gs=gridspec.GridSpec(6,1)

    # data projection
    dL = ccrs.PlateCarree()

    # map projection
    if mproj == 'area':
        cL = ccrs.Stereographic(
            central_longitude=clon,
            central_latitude =clat)

        ax = fig.add_subplot(1, 1, 1, projection=cL)
        #ax=plt.subplot(gs[:-1,:],projection=cL)

        # area extention
        ext = [wlon, elon, slat, nlat]
        ax.set_extent(ext, dL)

    else:
        if mproj == 'np':
            cL = ccrs.AzimuthalEquidistant(
                central_longitude=0,
                central_latitude=90)
            ext=[-180, 180, 15, 90]

        elif mproj == 'sp':
            cL = ccrs.AzimuthalEquidistant(
                central_longitude=0,
                central_latitude=-90)
            ext=[-180, 180, -90, -15]

        else:
            print("incorrect map projection")
            print("please choose area, np or sp")
            quit()

        ax = fig.add_subplot(1, 1, 1, projection=cL)        
        #ax=plt.subplot(gs[:-1,:],projection=cL)

        # field of view (round shape)
        ax.set_extent(ext, ccrs.PlateCarree())
        theta = np.linspace(0, 2*np.pi, 100)
        center, radius = [0.5, 0.5], 0.5
        verts = np.vstack([np.sin(theta), np.cos(theta)]).T
        circle = mpath.Path(verts * radius + center)
        ax.set_boundary(circle, transform=ax.transAxes)


    # topograph
    ax.coastlines(resolution='50m', linewidth=1.2, color='gray', zorder=3)
    #ax.add_feature(cftr.LAND, zorder=0, facecolor='lightgray')
    try:
        ax.add_feature(cftr.BORDERS, linestyle='-', 
                       linewidth=0.5, edgecolor='gray')
        ax.add_feature(cftr.LAKES, linestyle='-', 
                       linewidth=0.5, edgecolor='gray', facecolor='none')
        #ax.add_feature(cftr.STATES, linestyle='-', 
        #               linewidth=0.5, edgecolor='gray')
    except:
        print('cannot download data??')
        print('avoid to draw cftr.BORDERS and cftr.LAKES')

    ax.gridlines(xlocs=range(-180,181,15), ylocs=range(-90,91,15), draw_labels=False)
    
    return fig, ax, dL


def func_gcd(_lon1,_lat1,_lon2,_lat2) :
    """calculate great-circle distance [km]"""
    if _lon1 == _lon2 and _lat1 == _lat2 :
        return 0.
    else :
        lon1=_lon1*np.pi/180 ; lon2=_lon2*np.pi/180
        lat1=_lat1*np.pi/180 ; lat2=_lat2*np.pi/180                
        ds=np.arccos(np.sin(lat1)*np.sin(lat2)
                     +np.cos(lat1)*np.cos(lat2)*np.cos(abs(lon1-lon2)))
        return rr*ds 


def plot_vortices(df, ax, dL, ty, df2):

    for i in range(len(df)) :
        
        ilon, ilat = df['lon'].iloc[i], df['lat'].iloc[i]
        iso, iro = df['So'].iloc[i], df['ro'].iloc[i]
        bgs = df['Sb'].iloc[i]
        _m, _n = df['m'].iloc[i], df['n'].iloc[i]
        rto = np.round(df['SR'].iloc[i], 1)

        # noise reduction
        #if rto > 3.: continue
        #if iso < 5.: continue

        # color circle
        # trough: green, cutoff low: blue
        if ty == 'L':
            ec = 'blue'
            mk = 'v'
        elif ty == 'H':
            ec = 'red'
            mk = '^'

        # draw local min/maximum points
        ax.scatter(df2.lon, df2.lat, s=45, marker=mk, c=ec,
                       zorder=10, linewidths=0.8, edgecolors='w', transform=dL)

        if df['ex'].iloc[i] == 0:
            ec = 'green'

        # draw r_o with tissot's indicatrix
        ax.tissot(
            lons=ilon, lats=ilat, rad_km=iro,
            facecolor='None', edgecolor=ec, linewidth=2,#1.2,2.5            
            zorder=5)

        ## vector plot
        # vector (quiver) color
        vflag = True
        if bgs < 5.: vflag=False
        qc = 'white'
        if 10 <= bgs < 20 : qc='lightgray'
        if 20 <= bgs < 30 : qc='skyblue'
        if 30 <= bgs < 40 : qc='dodgerblue'
        if 40 <= bgs < 50 : qc='navy'
        if 50 <= bgs : qc='purple'

        # normalize vectors
        _m = _m / bgs
        _n = _n / bgs

        # make array for quiver
        a1, a2 = np.array([ilon]), np.array([ilat])
        a3, a4 = np.array([_m]), np.array([_n])
        if vflag:
            # main
            ax.quiver(a1, a2, a3, a4, angles='xy',
                      lw=0.8, edgecolor=ec, color=qc,
                      scale=22, width=0.01, headwidth=3.2,
                      headlength=3, headaxislength=3,
                      pivot='middle', zorder=7, transform=dL,)

        ## solid circle plot
        # circle color
        if 0 < iso < 10: col='w'
        if 10 <= iso < 20: col='lemonchiffon'
        if 20 <= iso < 30: col='sandybrown'
        if 30 <= iso < 40: col='sienna'
        if 40 <= iso : col='purple'

        # main
        ax.plot(ilon, ilat, markersize=7.5,
                marker='o', color=col,
                mec=ec, mew=0.8,
                transform=dL, zorder=9,)

        # transform coords for annot
        if ty == 'H':
            rto = np.round(df['SR'].iloc[i]*-1., 1)
        at_x, at_y = ax.projection.transform_point(ilon, ilat, src_crs=dL)
        ax.annotate(str(rto), xy=(at_x, at_y),
                    color='k', size=13, zorder=11)


def draw_figs(_z, ite, c, l, ty) :

    _z=np.where(_z==undef,np.nan,_z)

    lev = levs[l]

    _t, _, _ = _z.shape

    '''
    if c != -1:
        print(levs[l], f'{c}/{CPU}', ty)
    else:
        print(levs[l], f'mod/{CPU}', ty)
    '''
        
    for t in range(_t):

        f = _z[t]

        if c != -1:
            tt = init_time + datetime.timedelta(hours=(ite*c+t)*dt)
        else:
            tt = init_time + datetime.timedelta(hours=(ite+t)*dt)

    
    for t in range(_t):

        if c != -1:
            tt = init_time + datetime.timedelta(hours=(ite*c+t)*dt)
        else:
            tt = init_time + datetime.timedelta(hours=(ite+t)*dt)
        
        tm = f'{tt.year}{tt.month:02}'
        tn = f'{tm}{tt.day:02}{tt.hour:02}'
        tnn = f"{tt.hour:02}00 UTC {tt.day:02} {tt.strftime('%b')} {tt.year}"

        ## making blank map
        fig, ax, dL = make_blank()

        # draw Z contour
        # data prep.
        __z = _z[t, :, :].reshape((iy, ix))
        __un = np.where(__z==np.nan, 1, np.nan) # for undef hatching

        Z , lons = add_cyclic_point(__z, coord=lon)

        lev1 = np.arange(0, 20001, 100)
        _cont = ax.contour(lons, lat, Z, lev1,
                          colors='w', linewidths=2, transform=dL, zorder=4)#0.8
        _cont.clabel(fmt='%5.0f', fontsize=8)
        cont = ax.contour(lons, lat, Z, lev1,
                          colors='k', linewidths=1, transform=dL, zorder=4)#0.8
        cont.clabel(fmt='%5.0f', fontsize=8)

        ## local extremum points of AS
        
        df = pd.read_csv(f'{idir}/{tm}/V-{ty}-{tn}-{lev:04}.csv',)
        
        # local min points of z
        df2 = pd.read_csv(f'{idir}/{tm}/X-{ty}-{tn}-{lev:04}.csv',)

        # main
        plot_vortices(df, ax, dL, ty, df2)
             
        # draw AS envelope shade & contour
        a = open(f'{idir}/{tm}/AS-{ty}-{tn}-{lev:04}.grd', 'r')
        _as = np.fromfile(a, dtype='>f4').reshape((iy, ix))
        a.close
        _as = np.where(_as==undef, np.nan, _as)
        AS , _ = add_cyclic_point(_as, coord=lon)
        
        if ty == 'H':
            AS = AS*(-1)
            levels1 = range(-40, 1, 5)
            extend = 'min'
            cmap='Greys_r'
        if ty == 'L':
            levels1 = range(0, 41, 5)
            extend = 'max'
            cmap='Greys'
        ctf = ax.contourf(lons, lat, AS, levels1,
                          cmap=cmap, extend=extend,
                          transform=dL, zorder=1)
        ax.contour(lons, lat, AS, levels1, colors='w',
                   linewidths=1, transform=dL, zorder=2)

        # colorbar
        if True:
            divider = mpl_toolkits.axes_grid1.make_axes_locatable(ax)
            cax = divider.append_axes('right', '4%', pad='3%', axes_class=plt.Axes)
            cbar=fig.colorbar(ctf,cax=cax)
            cax.tick_params(labelsize=12)
            #fig.colorbar(ctf, orientation='horizontal')

        # undef hatching
        #if np.any(__un==1) :
        #    un, _ = add_cyclic_point(__un, coord=lon)
        #    ax.contourf(lons , lat, un, hatches=['//'],
        #                cmap='gray', extend='both', transform=dL)
                
        # title
        if ty == 'L':
            ax.set_title('Lows & $AS^{+}$ & $Z$ '+f'{lev}hPa', fontsize=10, loc='left')

        if ty == 'H':
            ax.set_title('Highs & $AS^{-}$ & $Z$ '+f'{lev}hPa', fontsize=10, loc='left')

        ax.set_title(str(tnn), fontsize=10,loc='right')
        
        
        plt.savefig(f'{odir}/{ty}-{tn}-{lev:04}-{mproj}.png')
        plt.cla()


def main(CPU):
    
    _zz, it = open_getit(infile)

    if CPU == None:
        CPU = 1
        
    rCPU = range(CPU)
    
    ite, mod = divmod(it, CPU)
    
    for i, l in enumerate(selected_lev):

        print(f'drawing @ {levs[l]} hPa ({i+1}/{len(selected_lev)})')

        # select a 2D field
        d2 = _zz[:, l, :, :].reshape((it, iy, ix))

        d2l = np.empty((CPU, ite, iy, ix))

        for c in rCPU:
            d2l[c,:,:,:] = d2[ite*c:ite*(c+1),:,:]
        d2l_mod = d2[ite*CPU:,:,:]

        
        if L_DETECT is True:

            with mp.Pool(CPU) as pool:
                exe = [pool.apply_async(draw_figs, (d2l[c], ite, c, l, 'L')) for c in rCPU]
                [ff.get() for ff in exe]

            draw_figs(d2l_mod, ite*CPU, -1, l, 'L')

        if H_DETECT is True:

            with mp.Pool(CPU) as pool:
                exe = [pool.apply_async(draw_figs, (d2l[c], ite, c, l, 'H')) for c in rCPU]
                [ff.get() for ff in exe]

            draw_figs(d2l_mod, ite*CPU, -1, l, 'H')

    print('FINISH!')

if __name__ == '__main__' :
    main(CPU)

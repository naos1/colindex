#!/usr/bin/env python3
'''v0.2'''
import numpy as np
import pygrib
import pandas as pd
import calendar

input_dir = '/mnt/data1/public_data/re-analysis/JRA-55/6hourly/anl_p125'
output_dir = 'JRA55-6hourly-month'

t = pd.date_range(start='2020/01/01 00',
                  end='2020/12/31 18',
                  freq='6H')

#print(t)
#quit()

for tt in t:
    print(tt)

    y = tt.year
    m = tt.month
    d = tt.day
    h = tt.hour

    num = f'{y}{m:02}{d:02}{h:02}'
    ym = f'{y}{m:02}'

    if d == 1 and h == 0:  # first day of month

        ld = calendar.monthrange(y, m)[1]  # last day of this month
        tm = len(pd.date_range(f'{y}-{m}-1 00',
                               f'{y}-{m}-{ld} 18',
                               freq='6H'))
        tmp2 = np.empty((tm, 37, 145, 288))
        i = 0  # for time dim.

    # read data
    tmp1 = []  # initialize
    with pygrib.open(f'{input_dir}/{ym}/anl_p125_hgt.{num}') as grbs:

        for grb in grbs:
            #print(grb)
            #print(grb.level)
            tmp1.append(grb.values)

        tmp2[i] = np.array(tmp1)

    i += 1

    if d == ld and h == 18:  # the last day of month

        print(f'Writing HGT{ym}.bin')

        hgt2 = np.array(tmp2)
        # flip z, y
        hgt2 = hgt2[:, ::-1, ::-1, :]
        
        hgt2 = np.array(hgt2, dtype='>f4')
        with open(f'{output_dir}/HGT{ym}.bin', 'wb') as out:
            out.write(hgt2)

print('Done!')

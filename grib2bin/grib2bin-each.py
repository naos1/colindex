#!/usr/bin/env python3
import numpy as np
import pygrib
import pandas as pd

input_dir = '/mnt/data1/public_data/re-analysis/JRA-55/6hourly/anl_p125'
output_dir = 'data'

t = pd.date_range(start='2020/05/01 00',
                  end='2020/08/31 18',
                  freq='6H')

print(t)
quit()

for tt in t:
    print(tt)

    num = f'{tt.year}{tt.month:02}{tt.day:02}{tt.hour:02}'
    ym = f'{tt.year}{tt.month:02}'

    with pygrib.open(f'{input_dir}/{ym}/anl_p125_hgt.{num}') as grbs:

        tmp1 = []
        for grb in grbs:
            #print(grb)
            #print(grb.level)
            tmp1.append(grb.values)

    tmp2 = np.array(tmp1)
    print('Writing file...')

    hgt2 = np.array(tmp2)
    # flip z, y
    hgt2 = hgt2[::-1, ::-1, :]

    hgt2 = np.array(hgt2, dtype='>f4')
    with open('{output_dir}/HGT{num}', 'wb') as out:
        out.write(hgt2)

print('Done!')

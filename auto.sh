#!/bin/bash

# sample for automatic and multi-processing loop

for lev in 200 500
do
	for ty in "L" "H"
	do

	./tracking_overlap.py detect track $lev $ty &

	done
done
wait

for lev in 200 500
do
	for ty in "L" "H"
	do

	./count_duration_case.py track count $lev $ty &

	done
done
wait
